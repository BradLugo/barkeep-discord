# barkeep-discord

Barkeep version 2, for Discord.

## What the crap is Barkeep?

Oh jeez. Long story.  

My friends and I met in Everquest II a **long** time ago, and raided together.  We used TeamSpeak for coordination because typing in game was awful.  Anyway, we like to drink.  And play video games.  Even when EQII died, we stuck together and played other things.  And drank more.  When it was time for everyone to take a shot, someone would yell it out.  Somehow, this got old, so we became more creative.  Each time someone in the group wanted everyone to drink, they played a sound off of some soundboard from a phone or laptop.  Funny stuff, usually.  

_Except for Chet._

He played the worst stuff.  Things only he laughed at.  Eventually Sqard complained.  And somehow, that was even worse than the sounds Chet played.  So, we needed a solution.

**Barkeep.**

Written in Bash, he frigging had it all.  He could browse a directory of over 1000 WAV files.  He could play them automatically in TeamSpeak.  **He remembered what he played**.  Well, not at first.  Sometimes he would play the same sound a few times over (thanks, not-really-random Bash built-in [$RANDOM](www.tldp.org/LDP/abs/html/randomvar.html).)  So, he was given a memory of the last few hundred things he played.  Ahh, that's better.

_So, what's the problem?_

Well, he was needy.
- Whole Linux VM for a house
- Full TeamSpeak client
- Bespoke software to emulate a finger for PTT
- Hacky audio device piping to give him vocal cords
- All the electrons he could choke down
- Ugh.

Also, he ran on my computer, so when I was away from home for a weekend, the group suffered.  Barkeep would not keep them full of social lubricant.  He would lay dormant.  Silent.  Waiting.

_Wait, why is barkeep going to Discord? Didn't you say you use TeamSpeak?_

Because, dude.  I'm cheap, TeamSpeak paywalled their SDK, and they wanted to datamine my project details for their marketing (and probably to try and drain my wallet in the most efficient way they could).  Fark that noise.

SO HERE'S A BETTER SOLUTION!

Discord releases their SDK and API and all of its documentation because they appreciate open source.  And now, instead of keeping this project private to avoid any liability or hurt fee-fees or whatever, I can open source it.

Anyway, here he is.  Bigger, better, more bot-y, better at telling Sqard (and probably Chet) to shut up.  Or he will be, at least.

**Thanks to my MMGA buds who have always seemed to gravitate together for laughs, drinks, and games even after all this time; and whose ideas made Barkeep possible.**

_This initial bot framework is based on the example `Airhorn` in the [discordgo project](https://github.com/bwmarrin/discordgo/tree/master/examples/airhorn)._
#!/bin/bash
# This runs on Ubuntu Xenial with PulseAudio and `jackd` connecting 
# output to input; this emulates a human speaker for TS to pick up.  
# It requires a full TeamSpeak3 client and a PTT key configured in
# `xdotool` (in the current case, `left ctrl`).  Callhistory is set to 300
# calls, for better call uniqueness.
 
trap 'killall -g -w ts3client_linux_amd64;killall -g -w pulseaudio;sudo killall -g jackd;sudo killall -g -w jackd;sudo killall -g -w jackd;exit 0' INT

echo
echo "---------------=STARTUP=-------------------"
echo "Killing any open instances of pulseaudio..."
pulseaudio -k
echo "Smooshing audio devices together..."
sudo jackd -R -P4 -dalsa -n4 -D -Chw:0 -Phw:0 2>/dev/null 1>/dev/null &
echo "Waiting for pulseaudio service to revive itself..."
sleep 5
echo "Starting TeamSpeak 3..."
echo "-------------------------------------------"
echo
LD_LIBRARY_PATH=/opt/ts3client:$LD_LIBRARY_PATH /opt/ts3client/ts3client_linux_amd64 2>/dev/null 1>/dev/null &
sleep 3
echo "-------------------------------------------------"
echo "Bartender operational.  Pouring first shot now..."
echo "-------------------------------------------------"
echo
sleep 7

WORKINGDIR=/opt/bartender
declare -a history
if [ -f $WORKINGDIR/callhistory ]
then
    history=($(cat $WORKINGDIR/callhistory))
else
    for i in {0..300}
    do
        history[$i]=0
    done
fi
echo
echo "--=CURRENT FILE PLAY HISTORY=---"
echo ${history[@]}
echo "--------------------------------"
echo

function checkarray() {
for i in {0..299}
do
    if [[ "${history[$i]}" = "$SELECTION" ]]
    then
        echo "Call number $SELECTION was already played! Selecting another."
        let "SELECTION = $RANDOM % $FILES + 1"
        checkarray
    fi
done
}

while true; do
    let "FILES = $(ls $WORKINGDIR/sounds-production | wc -w)"
    let "SELECTION = $RANDOM % $FILES + 1"
    checkarray
    for i in {299..0}
    do
        history[$(($i + 1))]=${history[$i]}
    done
    history[0]=$SELECTION
    echo ${history[@]} > $WORKINGDIR/callhistory
    export SELECTIONFILENAME=$(ls $WORKINGDIR/sounds-production | head -n $SELECTION | tail -n 1)
    echo "Playing file number $SELECTION of $FILES at $(date). Filename is $SELECTIONFILENAME"
    xdotool keydown Control_L
    sleep .5
    aplay $WORKINGDIR/sounds-production/$SELECTIONFILENAME 1>/dev/null 2>/dev/null
    sleep .5
    xdotool keyup Control_L
    let "SLEEPTIME = $RANDOM % 8 + 7"
    echo "Time to next call: $SLEEPTIME minutes."
    echo
    let "SLEEPTIME = $SLEEPTIME * 60"
    sleep $SLEEPTIME
done
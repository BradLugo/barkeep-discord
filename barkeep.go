package main

import (
	"crypto/rand"
	"encoding/binary"
	"flag"
	"io"
	"io/ioutil"
	"log"
	"math/big"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"github.com/bwmarrin/discordgo"
)

var (
	token    string
	err      error
	dir      string
	delayMin int64
	delayMax int64
	histLen  int
	guildID  string
	files    []os.FileInfo
	vc       *discordgo.VoiceConnection
)

//arg handling and validation
func init() {

	flag.StringVar(&token, "t", "", "Bot Token")
	flag.StringVar(&dir, "d", "", "Sound files directory")
	flag.Int64Var(&delayMin, "delay-min", 420, "Minimum time delay between calls (integer, s). Valid values: 1 - 900")
	flag.Int64Var(&delayMax, "delay-max", 840, "Maximum time delay between calls (integer, s). Valid values: 5 - 1800")
	flag.IntVar(&histLen, "history-length", 300, "Call history list length (integer). Valid values: 1 - 1000")
	flag.Parse()

	//token might be borked but should at least not be null
	if token == "" {
		log.Fatal("Token error: check that token is provided with '-t'")
	}

	//validate diff of delayMin & delayMax
	//they can be set to equal values if desired but min > max is invalid
	if delayMin > delayMax {
		log.Fatal("Error: value of min delay cannot be larger than max")
	}

	//validate delayMin value
	if delayMin < 1 || delayMin > 900 {
		log.Fatal("Invalid min delay value: see help")
	}

	//validate delayMax value
	if delayMax < 5 || delayMax > 1800 {
		log.Fatal("Invalid max delay value: see help")
	}

	//populate and validate sound files directory
	//grab list of directory entries
	//TODO add validation of filenames, cannot have spaces
	entries, err := parseDir(dir)
	if err != nil {
		log.Fatal("Cannot parse directory specified: ", err)
	}

	//list only dca files
	files := dcaFileFilter(entries)
	if len(files) == 0 {
		log.Fatal("No files found! Verify specified directory is valid.")
	}

	//validate histLen
	if histLen < 1 || histLen > 1000 {
		log.Fatal("Invalid history length: see help")
	}

	//histLen cannot be larger than number of playable sound files
	if histLen >= len(files) {
		log.Fatal("Invalid history length, cannot be larger than number of dca files in directory specified: ", len(files))
	}

}

func main() {

	//populate and validate sound files directory
	//grab list of directory entries
	//This currently also happens in init(), should probably make this more efficient
	entries, err := parseDir(dir)
	if err != nil {
		log.Fatal("Cannot parse directory specified: ", err)
	}

	//list only dca files
	files := dcaFileFilter(entries)
	if len(files) == 0 {
		log.Fatal("No files found! Verify specified directory is valid.")
	}

	//create new discord session
	bk, err := discordgo.New("Bot " + token)
	if err != nil {
		log.Fatal("Error creating Discord session: ", err)
	}

	//add discord event handlers
	bk.AddHandler(ready)
	bk.AddHandler(loginMessage)
	bk.AddHandler(getGuildID)

	//attempt to login using created discord session
	err = bk.Open()
	if err != nil {
		log.Fatal("Error opening Discord session: ", err)
	}

	//sleep for a time to give other operations time to complete
	//TODO change this to check for server response and/or populated *discordgo.Session object
	time.Sleep(1 * time.Second)

	//some diagnostic messages, beep boop
	log.Printf("Barkeep is ready to pour some drinks...")
	log.Print("guildID: ", guildID)

	guildState, err := bk.State.Guild(guildID)
	if err != nil {
		log.Fatal("Error retrieving guild ID: ", err)
	}

	//okay, go get that vc
	for _, channel := range guildState.Channels {

		//channel.Type 2 is equivalent to GUILD_VOICE
		//https://discordapp.com/developers/docs/resources/channel#channel-object-channel-types
		if channel.Name == "the bar" && channel.Type == 2 {
			vc, err = bk.ChannelVoiceJoin(guildState.ID, channel.ID, false, true)
			if err != nil {
				log.Print("Error joining voice channel: ", err)
			} else {
				log.Print("voice channel joined")
			}
		}
	}

	//play loop
	//TODO implement the rest of main() in signal / case logic
	go shotCall(files, vc)
	log.Print("play loop activated")

	//listen for termination events
	log.Print("waiting for term events")
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc

	//close voice connection on termination
	err = leaveVoiceChannel(vc)
	if err != nil {
		log.Fatal("Some error leaving voice channel - but program is stopping anyway")
	}

	//close discord session on termination
	bk.Close()
}

//////////////////////////////
////////// END MAIN //////////
//////////////////////////////

//set session state to 'ready'; triggered by event handler
func ready(s *discordgo.Session, event *discordgo.Ready) {

	s.UpdateStatus(0, "")
	log.Print("ready handler triggered")
}

//HOPEFULLY extract guild ID so that something frigging useful can be derived
//from the session state
//NO IM NOT ANGRY, WHY DO YOU ASK, NEVER MIND SHUT UP
func getGuildID(s *discordgo.Session, event *discordgo.GuildCreate) {

	//saves a string that is part of a struct of a bunch of strings and other
	//miscellaneousas types as a global variable string whose type is already
	//set to string because string.
	//string
	guildID = event.Guild.ID
	log.Print("getGuildID handler triggered")

	//ok bye
	return
}

//entrance message; triggered by event handler
func loginMessage(s *discordgo.Session, event *discordgo.GuildCreate) {

	for _, channel := range event.Guild.Channels {
		//if channel.ID == event.Guild.ID {  //use this for channel #general when ready

		log.Print("loginMessage handler triggered")
		//channel.Type 0 is equivalent to GUILD_TEXT
		//https://discordapp.com/developers/docs/resources/channel#channel-object-channel-types
		if channel.Name == "bartender-v2-dev" && channel.Type == 0 {
			_, _ = s.ChannelMessageSend(channel.ID, "THE DREAD PIRATE BARKEEP HAS ARRIVED")
			return
		}
	}
}

// sleep for randomized n seconds where smin <= n <= smax
//TODO rename function (timer is a type in time) and pull sleep command up to caller
func delayTimer(smin, smax int64) {

	delta := big.NewInt(smax - smin)
	random, _ := rand.Int(rand.Reader, delta)
	totalDelay := random.Int64() + smin
	sleepDuration := time.Duration(totalDelay)
	log.Print("delayTimer triggered, sleeping for ", totalDelay, " seconds")
	time.Sleep(sleepDuration * time.Second)
}

// get dem bytes into a buffer, yo
func loadFile(dir, filename string) ([][]byte, error) {

	buffer := make([][]byte, 0)

	filePath := strings.Join([]string{dir, filename}, "/")
	file, err := os.Open(filePath)
	if err != nil {
		return nil, err
	}

	//this should execute on return to caller, I hope
	defer file.Close()

	//opus frame length
	var frameLen int16

	for {
		// Read opus frame length from file.
		err = binary.Read(file, binary.LittleEndian, &frameLen)

		// If this is the end of the file, just return.
		if err == io.EOF || err == io.ErrUnexpectedEOF {
			err := file.Close()
			if err != nil {
				return nil, err
			}
			break
		}

		if err != nil {
			return nil, err
		}

		// Read encoded pcm from file.
		inBuf := make([]byte, frameLen)
		err = binary.Read(file, binary.LittleEndian, &inBuf)

		// Should not be any end of file errors
		if err != nil {
			log.Print("Error reading from dca file :", err)
			return nil, err
		}

		// Append encoded pcm data to the buffer.
		buffer = append(buffer, inBuf)
	}

	return buffer, nil
}

// disconnects from current voice connection
func leaveVoiceChannel(v *discordgo.VoiceConnection) error {

	return v.Disconnect()
}

// plays the selected audio
func playFile(v *discordgo.VoiceConnection, audioBuffer [][]byte) error {

	err := v.Speaking(true)
	if err != nil {
		return err
	}

	for _, buff := range audioBuffer {
		v.OpusSend <- buff
	}

	// add a small buffer so the end of the sound doesn't get cut off
	time.Sleep(50 * time.Millisecond)

	v.Speaking(false)

	return nil
}

// use a RNG to pick an element of the supplied list of files
func fileSelector(f []os.FileInfo) (string, error) {

	// this implementation of rand features a ceiling input, so we don't have
	// to use modulo # of files in list for selection
	lenList := big.NewInt(int64(len(f)))
	random, err := rand.Int(rand.Reader, lenList)
	if err != nil {
		return "", err
	}
	for k, entry := range f {
		if int64(k) == random.Int64() {
			return entry.Name(), nil
		}
	}

	return "", err
}

// parse directory specified by '-d'
func parseDir(d string) ([]os.FileInfo, error) {

	entries, err := ioutil.ReadDir(d)
	if err != nil {
		return entries, err
	}

	return entries, nil
}

// pull regular files ending in ".dca" into a new list
// yes, I know this is a horrible way to test for opus-encoded files.
// sshhhh.
func dcaFileFilter(entries []os.FileInfo) []os.FileInfo {

	// set capacity to number of detected entries to save time otherwise spent
	// on stack memory reallocation, efficiency!
	files := make([]os.FileInfo, 0, len(entries))

	//append on desired conditions to return var
	for _, entry := range entries {
		mode := entry.Mode()
		if mode.IsRegular() && strings.HasSuffix(entry.Name(), ".dca") {
			files = append(files, entry)
		}
	}

	return files
}

//wrapper for file player, for use in loop
func shotCall(files []os.FileInfo, vc *discordgo.VoiceConnection) {

	callHistory := make([]string, 0)

	//set first iteration marker
	py := 0

	//set history match control bit
	match := 0

	//assemble meta dir and history file path as strings
	metaDirPath := strings.Join([]string{dir, "meta"}, "/")
	histFilePath := strings.Join([]string{metaDirPath, "history.txt"}, "/")

	//create meta dir, noop if exists, err otherwise
	err := os.MkdirAll(metaDirPath, 0755)
	if err != nil {
		log.Fatal("Error creating meta directory: ", err)
	}

	//read in history file
	ch, err := ioutil.ReadFile(histFilePath)

	//try creating history file if not exists
	//TODO convert history from space-delimited to newline-delimited
	if err, ok := err.(*os.PathError); ok && err.Err == syscall.ENOENT {
		histFile, err := os.Create(histFilePath)
		if err != nil {
			log.Print("Error creating history.txt: ", err)
		}
		histFile.Close()
	} else if err != nil {
		log.Print("Error reading call history: ", err)
	}

	//hold history filenames in memory as slice
	if ch != nil {
		callHistory = strings.Split(string(ch), " ")
	}

	//call execution loop
	for {

		if py != 0 && match == 0 {

			//delayTimer is here, normal loop case
			//first iteration of loop should not delay
			delayTimer(delayMin, delayMax)
		}

		//grab random file from list
		file, err := fileSelector(files)
		if err != nil {
			log.Print("File selection failed: ", err)
		}

		//reset match control bit
		match = 0

		//check filename against callhistory, immediately loop back to
		//file selection if found
		for _, chEntry := range callHistory {

			if file == chEntry {
				log.Print("File ", file, " already played recently! Reselecting...")
				match = 1
			}
		}

		//reset to loop head if the file was a match
		if match == 1 {
			continue
		}

		//never gonna not let you know
		log.Print("Playing: ", file)

		//buffer selected file
		bufferedFile, err := loadFile(dir, file)
		if err != nil {
			log.Print("Error loading and/or buffering file: ", err)
			continue
		}

		//output buffer to discord audio chan
		err = playFile(vc, bufferedFile)
		if err != nil {
			log.Print("Error playing file from buffer: ", err)
			continue
		}

		//update callHistory, truncating slice to histLen if larger and popping oldest element
		if len(callHistory) >= histLen {
			callHistory = callHistory[1:histLen]
		}
		callHistory = append(callHistory, file)

		//convert new call history slice to []byte and write to history file
		newCallHistory := strings.Join(callHistory, " ")
		chByte := []byte(newCallHistory)
		ioutil.WriteFile(histFilePath, chByte, 0644)

		//increment play marker to 1 if first iteration
		// this should only happen once
		if py == 0 {
			py = 1
		}
	}
}
